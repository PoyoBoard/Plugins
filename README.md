# PoyoBoard Plugins
Additional plugins for PoyoBoard. Note that depending on their functionality you may need to install additional PHP extensions on your web server that aren't required for a standard PoyoBoard installation.

## Plugin Buckets
Each plugin is loaded into the system via lib/pluginloader.php and when a 'bucket' matching the name of a PHP file in the plugin directory is referenced in a file, the plugin data is called. Several common ones are listed below.

* init.php - initialisation (lib/common.php)
* finish.php - final page print (index.php)
TODO: list all of them here included in the stock software.

Plugins can also create their own buckets for other plugins to utilise.

## Disclaimer
We offer no guarantee for support for any plugins made for this software. They're provided as-is from their respective developers.